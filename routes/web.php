<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**************************************************************************
        Front End
 **************************************************************************/

Route::get('/', function(){
        return view('front.index');
});

Route::get('/profile/u/{name}', 'ProfileController@showProfile')->name('profile');
Route::get('/anime/det/{slug}', 'PagesController@animeDetail'); // target about function and returns anime detail page - ex: 'http://127.0.0.1:8000/anime/det/naruto'






/**************************************************************************
        Auth Routes for Front End
 **************************************************************************/
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );






/**************************************************************************
        API Points for Front End
 **************************************************************************/

 // Check Auth
//  Route::get('/api/v1/auth', 'UserController@getAuthUser');

// List All Users
Route::get('/api/v1/users', 'UserController@index');
Route::get('/api/v1/user/{id}', 'UserController@show');
// Route::get('/api/v1/profile/u/{name}', 'UserController@getUserByName');


// User Crud
Route::get('/api/v1/profile/u/{display_name}', 'UserController@getUserByName');
Route::post('/api/v1/profile/u/{display_name}/update', 'UserController@update');


// Bookmark Crud
// Route::get('/api/v1/bookmarks', 'BookmarkController@index');
Route::get('/api/v1/bookmarks/{id}', 'BookmarkController@getBookmarks');
Route::get('/api/v1/bookmark/test', 'BookmarkController@booky');
Route::post('/api/v1/bookmark', 'BookmarkController@bookmarkAnime');
Route::get('/api/v1/bookmark/check/{slug}', 'BookmarkController@checkBookmarkStatus');
Route::get('/api/v1/bookmark/favoriteBookmark/{id}', 'BookmarkController@favoriteBookmark');
Route::get('/api/v1/bookmark/delete/{id}', 'BookmarkController@destroy');


// Anime Routes
Route::get('/api/v1/animes', 'AnimeController@index');
Route::get('/api/v1/anime/det/{slug}', 'AnimeController@getAnimeBySlug');


/**************************************************************************
        Backend Portal (Admin)
 **************************************************************************/

// Admin Home
Route::get('/admin', 'AdminController@index')->middleware('auth');

// User Crud
Route::get('/admin/users', 'AdminController@users')->middleware('auth');
Route::get('/admin/users/edit/{id}', 'AdminController@showUser')->middleware('auth');
Route::get('/admin/users/create', 'AdminController@createUser')->middleware('auth');
Route::post('/admin/users/create',['uses' => 'AdminController@storeUser','as'  => 'storeUser'])->middleware('auth');
Route::post('/admin/users/update/{id}',['uses' => 'AdminController@updateUser','as'  => 'updateUser'])->middleware('auth');
Route::get('/admin/users/delete/{id}',['uses' => 'AdminController@destroyUser','as'  => 'destroyUser'])->middleware('auth');

// Anime Crud
Route::get('/admin/anime', 'AdminController@anime')->middleware('auth');
Route::get('/admin/anime/create', 'AdminController@createAnime')->middleware('auth');
Route::post('/admin/anime/create', ['uses' => 'AdminController@storeAnime', 'as' => 'storeAnime'])->middleware('auth');
Route::get('/admin/anime/edit/{id}', 'AdminController@showAnime')->middleware('auth');
Route::post('/admin/anime/update/{id}',['uses' => 'AdminController@updateAnime','as'  => 'updateAnime'])->middleware('auth');
Route::get('/admin/anime/delete/{id}',['uses' => 'AdminController@destroyAnime','as'  => 'destroyAnime'])->middleware('auth');

// Episode Crud
Route::get('/admin/anime/episodes', 'AdminController@episode')->middleware('auth');
Route::get('/admin/anime/episodes/create', 'AdminController@createEpisode')->middleware('auth');
Route::post('/admin/anime/episodes/create', 'AdminController@storeEpisode')->middleware('auth');


// Category Crud
Route::get('/admin/anime/categories', 'AdminController@categories')->middleware('auth');
Route::get('/admin/anime/category/create', 'AdminController@createCategory')->middleware('auth');
Route::post('/admin/anime/category/create', ['uses' => 'AdminController@storeCategory', 'as' => 'storeCategory'])->middleware('auth');
Route::get('/admin/anime/category/edit/{id}', 'AdminController@showCategory')->middleware('auth');
Route::post('/admin/anime/update/category/{id}',['uses' => 'AdminController@updateCategory','as'  => 'updateCategory'])->middleware('auth');
Route::get('/admin/anime/delete/category/{id}',['uses' => 'AdminController@destroyCategory','as'  => 'destroyCategory'])->middleware('auth');