<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            {{-- <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
                </div>
                <!-- /input-group -->
            </li> --}}
            <li><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li><a href="{{ url('/admin/users') }}">Users</a></li>
            <li><a href="{{ url('/admin/anime') }}">Anime</a></li>
            <li><a href="{{ url('/admin/anime/episodes') }}">Anime Episodes</a></li>
            <li><a href="{{ url('/admin/anime/categories') }}">Anime Categories</a></li>
            <li><a href="{{ url('/admin/scp') }}">Scrapper Manager</a></li>
            <li><a href="{{ url('/admin/episodes/brk-rpt') }}">Broken Episodes</a></li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>