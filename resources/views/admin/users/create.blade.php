@extends('layouts.admin')

@section('content')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Add A New User</h3>
            <p>Create a brand new user and add them to this site.</p>
            <a href="{{ url('/admin/users') }}" class="btn btn-default btn-sm">List</a>
            <a href="{{ url('/admin/users/create') }}" class="btn btn-default btn-sm btn-active">Add</a>
          </div>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-md-4">
          @include('inc.messages')
        </div>
    </div>

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add A New User
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <form role="form" enctype="multipart/form-data" method="POST" action="{{ route('storeUser')}}">
                                    <div class="form-group">
                                        <label for="display_name">Display Name</label>
                                        <input class="form-control" type="text" name="display_name" value="{{ old('name') }}" id="display_name" placeholder="Display Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input class="form-control" type="email" name="email" value="{{ old('email') }}" id="email" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input class="form-control" type="password" name="password" value="{{ old('password') }}" id="password" placeholder="Password">
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-default">Create User</button>
                                    {{-- <button type="reset" class="btn btn-default">Reset Button</button> --}}
                                  </div>
                              </form>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

@endsection