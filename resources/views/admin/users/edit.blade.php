@extends('layouts.admin')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Edit User - {{ $user->display_name }}</h3>
            <a href="{{ url('/admin/users') }}" class="btn btn-default btn-sm">List</a>
            <a href="{{ url('/admin/users/create') }}" class="btn btn-default btn-sm btn-active">Edit</a>
          </div>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-md-4">
          @include('inc.messages')
        </div>
    </div>

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                       {{ $user->id }} - {{ $user->display_name }}  | Last Updated: <span style="color: blue;">{{ $user->updated_at->format('l - F j, Y H:i') }}</span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <form role="form" enctype="multipart/form-data" method="POST" action="{{ route('updateUser', ['id' => $user->id]) }}">
                                    {{-- {{ csrf_field() }} --}}
                                    {{-- Spoof --}}
                                    {{-- {{ method_field('PUT') }} --}}
                                    <div class="form-group">
                                        <label for="name">Display Name</label>
                                        <input class="form-control" type="text" name="display_name" value="{{ $user->display_name }}" id="display_name" placeholder="Display Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input class="form-control" type="text" name="name" value="{{ $user->name }}" id="name" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input class="form-control" type="email" name="email" value="{{ $user->email }}" id="email" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="bio">Bio Description</label>
                                        <textarea class="form-control" cols="1" rows="10" name="bio" id="bio">{{ $user->bio }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Avatar Image</label>
                                        <input type="file" name="avatar_img">
                                    </div>
                                    <div class="form-group">
                                        <label>Cover Image</label>
                                        <input type="file" name="cover_img">
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-default" onclick="return confirm('Ready to confirm changes?')">Update User</button>
                                    {{-- <button type="reset" class="btn btn-default">Reset Button</button> --}}
                                  </div>
                                  <!-- /.col-lg-6 (nested) -->
                                  <div class="col-lg-5 col-md-offset-1">
                                    <img src="/storage/user_avatar_images/{{ $user->avatar }}" style="width: 150px;height: 150px;border-radius: 999em;border: 2px solid;"/>
                                  </div>
                                  <div class="col-lg-5 col-md-offset-1">
                                    <img src="/storage/user_cover_images/{{ $user->cover }}" style="width: 100%;height: auto;border: 2px solid;margin-top: 14px;"/>
                                  </div>
                              </form>
                              @if ($user->role == 2)
                                {!!Form::open(['action' => ['AdminController@destroyUser', 'id' => $user->id], 'method' => 'GET'])!!}
                                        {{Form::hidden('_method', 'DELETE')}}
                                        {{Form::submit('Delete User', ['class' => 'btn btn-outline btn-danger', 'onclick' => 'return confirm("Are you sure?")'])}}
                                {!! Form::close() !!}
                                @else
                                <button class="btn btn-outline btn-danger" title="cannot delete admin" disabled>Delete User</button>
                              @endif
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->
@endsection