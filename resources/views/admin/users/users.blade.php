@extends('layouts.admin')

@section('content')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Users</h3>
            <a href="{{ url('/admin/users') }}" class="btn btn-default btn-sm btn-active">List</a>
            <a href="{{ url('/admin/users/create') }}" class="btn btn-default btn-sm">Add</a>
            <ul class="subsubsub">
              <li class="all">All <span class="count">({{ count($users) }})</span> |</li>
              <li class="administrator">Administrator  <span class="count">({{ $admincount }})</span> |</li>
              <li class="truebasicuser">Basic User <span class="count">({{ $usercount }})</span></li>
            </ul>
          </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    All Users
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Avatar</th>
                                <th>Display Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>View</th>
                                <th>Date Created</th>
                                <th>Date Updated</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user )
                            <tr class="odd gradeX">
                            <td><img src="/storage/user_avatar_images/{{ $user->avatar }}" style="width: 35px;height: 35px;border-radius: 999em;border: 2px solid;"/></td>
                            <td>{{ $user->display_name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->role }}</td>
                            <td class="center">
                                <a href="/admin/users/edit/{{ $user->id }}" class="btn btn-outline btn-primary btn-sm">Edit</a>
                                <a href="/profile/u/{{ $user->display_name }}" target="_blank" class="btn btn-outline btn-primary btn-sm">Live</a>
                            </td>
                            <td>{{ $user->created_at->format('l - F j, Y H:i') }}</td>
                            <td>{{ $user->updated_at->format('l - F j, Y H:i') }}</td>
                            @if ($user->role == 1)
                            <td class="center"><button class="btn btn-default btn-sm" title="cannot delete admin" disabled>Remove</button></td>
                              @else
                                <td class="center">
                                {!!Form::open(['action' => ['AdminController@destroyUser', 'id' => $user->id], 'method' => 'GET'])!!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    {{Form::submit('Remove', ['class' => 'btn btn-outline btn-danger btn-sm', 'onclick' => 'return confirm("Are you sure?")'])}}
                                {!! Form::close() !!}
                                </td>
                              @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

</div>
<!-- /#page-wrapper -->

@endsection