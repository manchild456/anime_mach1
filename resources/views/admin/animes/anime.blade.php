@extends('layouts.admin')

@section('content')

<div id="page-wrapper">
  <div class="row">
      <div class="col-lg-12">
        <div class="page-header">
          <h3>Anime (List)</h3>
          <a href="{{ url('/admin/anime') }}" class="btn btn-default btn-sm btn-active">List</a>
          <a href="{{ url('/admin/anime/create') }}" class="btn btn-default btn-sm">Add</a>
        </div>
      </div>
      <!-- /.col-lg-12 -->
  </div>
      <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    All Anime
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Categories</th>
                                <th>Status</th>
                                <th>View</th>
                                <th>Remove</th>
                                <th>Date Added</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($animes as $anime )
                            <tr class="odd gradeX">
                            <td>{{ $anime->title }}</td>
                            <td>
                                @if (count($anime->categories) > 0)
                                    @foreach ($anime->categories as $category)
                                        {{-- <a href="{{ $category }}">{{ $category }}</a> --}}
                                        {{ $category }}
                                        @if (!$loop->last),@endif
                                    @endforeach
                                @endif
                                {{-- @if (count($anime->categories) > 0)
                                    @foreach ($anime->Categories as $categories )
                                        {{ $categories->name }}
                                    @endforeach
                                @endif --}}
                            </td>
                            <td>{{ $anime->status }}</td>
                            <td class="center">
                                <a href="/anime/det/{{ $anime->slug }}/{{ $anime->id }}" class="btn btn-outline btn-primary btn-sm">Live</a>
                                <a href="/admin/anime/edit/{{ $anime->id }}" class="btn btn-outline btn-primary btn-sm">Edit</a>
                            </td>
                                <td class="center">
                                {!!Form::open(['action' => ['AdminController@destroyAnime', 'id' => $anime->id], 'method' => 'GET'])!!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    {{Form::submit('Remove', ['class' => 'btn btn-outline btn-danger btn-sm', 'onclick' => 'return confirm("Are you sure?")'])}}
                                {!! Form::close() !!}
                                </td>
                            <td>{{ $anime->created_at->format('l - F j, Y H:i') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@endsection