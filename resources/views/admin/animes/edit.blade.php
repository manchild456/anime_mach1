@extends('layouts.admin')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Anime (Edit) - {{ $anime->title }}</h3>
            <a href="{{ url('/admin/anime') }}" class="btn btn-default btn-sm">List</a>
            <a href="{{ url('/admin/anime/create') }}" class="btn btn-default btn-sm">Add</a>
            <a href="/admin/anime/{{ $anime->id }}/eps" class="btn btn-default btn-sm">List Episodes</a>
            <a href="{{ url('/admin/anime/create/eps') }}" class="btn btn-default btn-sm">Add Episode</a>
            <a href="/anime/det/{{ $anime->slug }}/{{ $anime->id }}" target="_blank" class="btn btn-outline btn-primary btn-sm">Anime Preview</a>
          </div>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-md-4">
          @include('inc.messages')
        </div>
    </div>
      <!-- /.row -->
  <div class="row">
      <div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                 Last Updated on: <span style="color: blue;">{{ $anime->updated_at->format('l - F j, Y H:i') }}</span>
              </div>
              <div class="panel-body create-new-anime">
                  <div class="row">
                      <div class="col-lg-4">
                          <form role="form" enctype="multipart/form-data" method="POST" action="{{ route('updateAnime', ['id' => $anime->id]) }}">
                              <div class="form-group">
                                  <label>Title<span style="color: #ff0000;" title="Required">*</span><span data-toggle="tooltip" data-placement="right" title="Name of anime" style=""> [?]</span></label>
                                  <input class="form-control" type="text" name="title" value="{{ $anime->title }}" id="title">
                              </div>
                              <div class="form-group">
                                  <label for="slug">Slug<span style="color: #ff0000;" title="Required">*</span>
                                    <span data-toggle="tooltip" data-placement="right" title="URL of anime detail page" style=""> [?]</span>
                                  </label>
                                  <input class="form-control" name="slug" value="{{ $anime->slug }}" id="slug" placeholder="Enter Slug">
                                  <p class="help-block">https://trueanimeclub.com/slug-name</p>
                              </div>
                                <div class="form-group">
                                  <label for="title_english">Title (english)<span data-toggle="tooltip" data-placement="right" title="english name of anime" style=""> [?]</span></label>
                                  <input class="form-control" type="text" name="title_english" value="{{ $anime->title_english }}" id="title_english">
                                </div>
                                  <div class="form-group">
                                  <label for="title_synonyms">Title (synonyms)<span data-toggle="tooltip" data-placement="right" title="synonym names of anime" style=""> [?]</span></label>
                                  <input class="form-control" type="text" name="title_synonyms" value="{{ $anime->title_synonyms }}" id="title_synonyms">
                                </div>
                                <div class="form-group">
                                  <label for="title_japanese">Title (japanese)<span data-toggle="tooltip" data-placement="right" title="japanese name of anime" style=""> [?]</span></label>
                                  <input class="form-control" type="text" name="title_japanese" value="{{ $anime->title_japanese }}" id="title_japanese">
                                </div>
                                <div class="form-group">
                                  <label for="description">Description<span data-toggle="tooltip" data-placement="right" title="Brief story of this anime" style=""> [?]</span></label>
                                  <textarea class="form-control" cols="1" rows="10" name="description" id="description">{{ $anime->description }}</textarea>
                                </div>
                          </div>
                          <div class="col-lg-5 col-md-offset-1">
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label>Anime Poster <span data-toggle="tooltip"  data-placement="right" title="poster image of this anime"> [?]</span></label>
                                          <input type="file" name="anime_poster">
                                          <img src="/storage/anime_poster_images/{{ $anime->anime_poster }}" style="max-width: 150px;max-height: 150px;border: 2px solid;margin-top: 14px;"/>
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label>Anime Cover <span data-toggle="tooltip" data-placement="right" title="cover image of this anime"> [?]</span></label>
                                          <input type="file" name="anime_cover">
                                          <img src="/storage/anime_cover_images/{{ $anime->anime_cover }}" style="width: 100%;height: auto;border: 2px solid;margin-top: 14px;"/>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="trailer_url">Trailer Video<span data-toggle="tooltip" data-placement="right" title="trailer video of anime (URL)" style=""> [?]</span></label>
                                        <input class="form-control" type="text" name="trailer_url" value="{{ $anime->trailer_url }}" id="trailer_url" placeholder="trailer video of anime (URL)">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="status">Status<span data-toggle="tooltip" data-placement="right" title="current anime status" style=""> [?]</span></label>
                                        <select name="status" value="{{ $anime->status }}" class="form-control" style="width:200px;">
                                            @if ($anime->status == 'Ongoing')
                                                <option selected value="Ongoing">Ongoing</option>
                                                <option value="Completed">Completed</option>
                                                <option value="Halt">Halt</option>
                                            @elseif($anime->status == 'Completed')
                                                <option selected value="Completed">Completed</option>
                                                <option value="Ongoing">Ongoing</option>
                                                <option value="Halt">Halt</option>
                                            @elseif($anime->status == 'Halt')
                                                <option selected value="Halt">Halt</option>
                                                <option value="Completed">Completed</option>
                                                <option value="Ongoing">Ongoing</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Rating<span data-toggle="tooltip" data-placement="right" title="Content rating of anime" style=""> [?]</span></label>
                                        <select name="rating" value="{{ $anime->rating }}" class="form-control" style="width:200px;">
                                            @if ($anime->rating == 'PG-13 - Teens 13 or older')
                                                <option selected value="PG-13 - Teens 13 or older">PG-13 - Teens 13 or older</option>
                                                <option value="R+ - Mild Nudity">R+ - Mild Nudity</option>
                                                <option value="R - 17+ (violence & profanity)">R - 17+ (violence & profanity)</option>
                                                <option value="G - All Ages">G - All Ages</option>
                                                <option value="PG - Children">PG - Children</option>
                                                <option value="None">None</option>
                                            @elseif($anime->rating == 'R+ - Mild Nudity')
                                                <option selected value="R+ - Mild Nudity">R+ - Mild Nudity</option>
                                                <option value="PG-13 - Teens 13 or older">PG-13 - Teens 13 or older</option>
                                                <option value="R - 17+ (violence & profanity)">R - 17+ (violence & profanity)</option>
                                                <option value="G - All Ages">G - All Ages</option>
                                                <option value="PG - Children">PG - Children</option>
                                                <option value="None">None</option>
                                            @elseif($anime->rating == 'R - 17+ (violence & profanity)')
                                                <option selected value="R - 17+ (violence & profanity)">R - 17+ (violence & profanity)</option>
                                                <option value="PG-13 - Teens 13 or older">PG-13 - Teens 13 or older</option>
                                                <option value="R+ - Mild Nudity">R+ - Mild Nudity</option>
                                                <option value="G - All Ages">G - All Ages</option>
                                                <option value="PG - Children">PG - Children</option>
                                                <option value="None">None</option>
                                            @elseif($anime->rating == 'G - All Ages')
                                                <option selected value="G - All Ages">G - All Ages</option>
                                                <option value="PG-13 - Teens 13 or older">PG-13 - Teens 13 or older</option>
                                                <option value="R+ - Mild Nudity">R+ - Mild Nudity</option>
                                                <option value="R - 17+ (violence & profanity)">R - 17+ (violence & profanity)</option>
                                                <option value="PG - Children">PG - Children</option>
                                                <option value="None">None</option>
                                            @elseif($anime->rating == 'PG - Children')
                                                <option selected value="PG - Children">PG - Children</option>
                                                <option value="PG-13 - Teens 13 or older">PG-13 - Teens 13 or older</option>
                                                <option value="R+ - Mild Nudity">R+ - Mild Nudity</option>
                                                <option value="R - 17+ (violence & profanity)">R - 17+ (violence & profanity)</option>
                                                <option value="G - All Ages">G - All Ages</option>
                                                <option value="None">None</option>
                                            @elseif($anime->rating == 'None')
                                                <option selected value="None">None</option>
                                                <option value="PG - Children">PG - Children</option>
                                                <option value="PG-13 - Teens 13 or older">PG-13 - Teens 13 or older</option>
                                                <option value="R+ - Mild Nudity">R+ - Mild Nudity</option>
                                                <option value="R - 17+ (violence & profanity)">R - 17+ (violence & profanity)</option>
                                                <option value="G - All Ages">G - All Ages</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Related Series<span data-toggle="tooltip" data-placement="right" title="select all anime franchise from list (hold down windows key to multiselect)" style=""> [?]</span></label>
                                        <select multiple="multiple" name="related_series[]" class="form-control">
                                            @foreach ($anime_list as $related_anime )
                                                @if ($related_anime->id == $anime->id )
                                                    <option disabled value="{{ $related_anime->id }}">
                                                        {{ $related_anime->title }}
                                                    </option> 
                                                {{-- @elseif(isset($related_anime->id, $anime->related_series))
                                                    <option selected="selected" value="{{ $related_anime->id }}">
                                                        {{ $related_anime->title }}
                                                    </option> --}}
                                                @else
                                                    <option value="{{ $related_anime->id }}">
                                                        {{ $related_anime->title }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="categories">Categories<span data-toggle="tooltip" data-placement="right" title="select all categories for this anime" style=""> [?]</span></label>
                                        <select multiple="multiple" name="categories[]" id="categories" class="form-control">
                                            @foreach ($category_list as $category )
                                                {{-- @if ($category->id == $anime->categories->id )
                                                    <option disabled value="{{ $category->id }}">
                                                        {{ $category->name }}
                                                    </option>  --}}
                                                {{-- @if(isset($category, $anime->categories))
                                                    <option selected="selected" value="{{ $category->name }}">
                                                        {{ $category->name }}
                                                    </option>
                                                @endif --}}
                                                    <option value="{{ $category->name }}">
                                                        {{ $category->name }}
                                                    </option>
                                            @endforeach
                                        </select>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="aired">Aired<span data-toggle="tooltip" data-placement="right" title="anime aired date" style=""> [?]</span></label>
                                        <input class="form-control" type="text" name="aired" value="{{ $anime->aired }}" id="aired">
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Type<span data-toggle="tooltip" data-placement="right" title="tv, or movie, or ova, or special etc.." style=""> [?]</span></label>
                                        <input class="form-control" type="text" name="type"  maxlength="250" value="{{ $anime->type }}" id="type">
                                    </div>
                                    <div class="form-group">
                                        <label for="episode_count">Episodes #<span data-toggle="tooltip" data-placement="right" title="number of episodes for this anime" style=""> [?]</span></label>
                                        <input class="form-control" type="text" name="episode_count"  maxlength="250" value="{{ $anime->episode_count }}" id="episode_count">
                                    </div>
                                    <div class="form-group">
                                        <label for="duration">Duration<span data-toggle="tooltip" data-placement="right" title="time length of each anime episode" style=""> [?]</span></label>
                                        <input class="form-control" type="text" name="duration" value="{{ $anime->duration }}" id="duration">
                                    </div>
                                    <div class="form-group">
                                        <label for="isHot">is Hot<span data-toggle="tooltip" data-placement="right" title="is this anime hot?" style=""> [?]</label>
                                        <label class="checkbox-inline">
                                            @if ($anime->isHot == 1)
                                               <input id="isHot" name="isHot" value="1" checked type="checkbox"> Yes
                                            @else
                                                <input id="isHot" name="isHot" value="1" type="checkbox"> No
                                            @endif
                                        </label>
                                    </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-outline btn-primary">Update Anime</button>
                            {{-- <button type="reset" class="btn btn-default">Reset Button</button> --}}
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
@endsection