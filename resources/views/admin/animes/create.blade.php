@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
            <div class="row">
               <div class="col-lg-12">
                <div class="page-header">
                  <h3>Create New Anime</h3>
                    <a href="{{ url('/admin/anime') }}" class="btn btn-default btn-sm">List</a>
                    <a href="{{ url('/admin/anime/create') }}" class="btn btn-default btn-sm btn-active">Add</a>
                </div>
              </div>
        <!-- /.col-lg-12 -->
        <div class="col-md-4">
          @include('inc.messages')
        </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create New Anime
                        </div>
                        <div class="panel-body create-new-anime">
                        <div class="row">
                            <div class="col-lg-4">
                                <form role="form" enctype="multipart/form-data" method="POST" action="{{ route('storeAnime')}}">
                                    <div class="form-group">
                                        <label>Title<span style="color: #ff0000;" title="Required">*</span><span data-toggle="tooltip" data-placement="right" title="Name of anime" style=""> [?]</span></label>
                                        <input class="form-control" type="text" name="title" value="{{ old('title') }}" id="title">
                                    </div>
                                    <div class="form-group">
                                        <label for="slug">Slug<span style="color: #ff0000;" title="Required">*</span>
                                            <span data-toggle="tooltip" data-placement="right" title="URL of anime detail page" style=""> [?]</span>
                                        </label>
                                        <input class="form-control" name="slug" value="{{ old('slug') }}" id="slug" placeholder="Enter Slug">
                                        <p class="help-block">https://trueanimeclub.com/slug-name</p>
                                    </div>
                                        <div class="form-group">
                                        <label for="title_english">Title (english)<span data-toggle="tooltip" data-placement="right" title="english name of anime" style=""> [?]</span></label>
                                        <input class="form-control" type="text" name="title_english" value="{{ old('title_english') }}" id="title_english">
                                        </div>
                                        <div class="form-group">
                                        <label for="title_synonyms">Title (synonyms)<span data-toggle="tooltip" data-placement="right" title="synonym names of anime" style=""> [?]</span></label>
                                        <input class="form-control" type="text" name="title_synonyms" value="{{ old('title_synonyms') }}" id="title_synonyms">
                                        </div>
                                        <div class="form-group">
                                        <label for="title_japanese">Title (japanese)<span data-toggle="tooltip" data-placement="right" title="japanese name of anime" style=""> [?]</span></label>
                                        <input class="form-control" type="text" name="title_japanese" value="{{ old('title_japanese') }}" id="title_japanese">
                                        </div>
                                        <div class="form-group">
                                        <label for="description">Description<span data-toggle="tooltip" data-placement="right" title="Brief story of this anime" style=""> [?]</span></label>
                                        <textarea class="form-control" cols="1" rows="10" name="description" id="description">{{ old('description') }}</textarea>
                                        </div>
                                </div>
                                <div class="col-lg-5 col-md-offset-1">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Anime Poster <span data-toggle="tooltip"  data-placement="right" title="poster image of this anime"> [?]</span></label>
                                                <input type="file" name="anime_poster">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Anime Cover <span data-toggle="tooltip" data-placement="right" title="cover image of this anime"> [?]</span></label>
                                                <input type="file" name="anime_cover">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="trailer_url">Trailer Video<span data-toggle="tooltip" data-placement="right" title="trailer video of anime (URL)" style=""> [?]</span></label>
                                                <input class="form-control" type="text" name="trailer_url" value="{{ old('trailer_url') }}" id="trailer_url" placeholder="trailer video of anime (URL)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="status">Status<span data-toggle="tooltip" data-placement="right" title="current anime status" style=""> [?]</span></label>
                                                <select name="status" value="{{ old('status') }}" class="form-control" style="width:200px;">
                                                    <option value="Ongoing">Ongoing</option>
                                                    <option value="Completed">Completed</option>
                                                    <option value="Halt">Halt</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="status">Rating<span data-toggle="tooltip" data-placement="right" title="Content rating of anime" style=""> [?]</span></label>
                                                <select name="rating" value="{{ old('rating') }}" class="form-control" style="width:200px;">
                                                    <option value="PG-13 - Teens 13 or older">PG-13 - Teens 13 or older</option>
                                                    <option value="R+ - Mild Nudity">R+ - Mild Nudity</option>
                                                    <option value="R - 17+ (violence & profanity)">R - 17+ (violence & profanity)</option>
                                                    <option value="G - All Ages">G - All Ages</option>
                                                    <option value="PG - Children">PG - Children</option>
                                                    <option value="None">None</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Related Series<span data-toggle="tooltip" data-placement="right" title="select all anime franchise from list (hold down windows key to multiselect)" style=""> [?]</span></label>
                                                <select multiple="multiple" name="related_series[]" class="form-control">
                                                    @foreach ($anime_list as $related_anime )
                                                        <option value="{{ $related_anime->id }}">
                                                            {{ $related_anime->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="categories">Categories<span data-toggle="tooltip" data-placement="right" title="select all categories for this anime" style=""> [?]</span></label>
                                                <select multiple="multiple" name="categories[]" id="categories" class="form-control">
                                                    {{-- <option>1</option> --}}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="aired">Aired<span data-toggle="tooltip" data-placement="right" title="anime aired date" style=""> [?]</span></label>
                                                <input class="form-control" type="text" name="aired" value="{{ old('aired') }}" id="aired">
                                            </div>
                                            <div class="form-group">
                                                <label for="type">Type<span data-toggle="tooltip" data-placement="right" title="tv, or movie, or ova, or special etc.." style=""> [?]</span></label>
                                                <input class="form-control" type="text" name="type"  maxlength="250" value="{{ old('type') }}" id="type">
                                            </div>
                                            <div class="form-group">
                                                <label for="episode_count">Episodes #<span data-toggle="tooltip" data-placement="right" title="number of episodes for this anime" style=""> [?]</span></label>
                                                <input class="form-control" type="text" name="episode_count"  maxlength="250" value="{{ old('episode_count') }}" id="episode_count">
                                            </div>
                                            <div class="form-group">
                                                <label for="duration">Duration<span data-toggle="tooltip" data-placement="right" title="time length of each anime episode" style=""> [?]</span></label>
                                                <input class="form-control" type="text" name="duration" value="{{ old('duration') }}" id="duration">
                                            </div>
                                            <div class="form-group">
                                                <label for="isHot">is Hot<span data-toggle="tooltip" data-placement="right" title="is this anime hot?" style=""> [?]</label>
                                                <label class="checkbox-inline">
                                                    <input id="isHot" name="isHot" value="1" type="checkbox"> Yes
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-outline btn-primary">Add Anime</button>
                                    {{-- <button type="reset" class="btn btn-default">Reset Button</button> --}}
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
        <!-- /#page-wrapper -->
@endsection