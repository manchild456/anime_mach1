@extends('layouts.admin')

@section('content')

<div id="page-wrapper">
  <div class="row">
      <div class="col-lg-12">
        <div class="page-header">
          <h3>Categories (List)</h3>
          <a href="{{ url('/admin/anime/categories') }}" class="btn btn-default btn-sm btn-active">List</a>
          <a href="{{ url('/admin/anime/category/create') }}" class="btn btn-default btn-sm">Add</a>
        </div>
      </div>
      <!-- /.col-lg-12 -->
  </div>
      <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    All Categories
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    {{ $categories->links() }}
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                              <th style="width: 250px;">Name</th>
                              <th>description</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($categories as $category )
                            <tr class="odd gradeX">
                            <td><a href="/admin/anime/category/edit/{{ $category->id }}">{{ $category->name }}</a></td>
                            <td> - {{ $category->description }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $categories->links() }}
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@endsection