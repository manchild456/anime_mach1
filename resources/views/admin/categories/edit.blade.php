@extends('layouts.admin')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Edit Category - {{ $category->name }}</h3>
            <a href="{{ url('/admin/anime/categories') }}" class="btn btn-default btn-sm">List</a>
            <a href="/admin/anime/category/edit/{{ $category->id }}" class="btn btn-default btn-sm btn-active">Edit</a>
            <a href="{{ url('/admin/anime/category/create') }}" class="btn btn-default btn-sm">Create</a>
          </div>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-md-4">
          @include('inc.messages')
        </div>
    </div>

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                      @if ($category->updated_at)
                        {{ $category->id }} - {{ $category->name }}  | Last Updated on: <span style="color: blue;">{{ $category->updated_at->format('l - F j, Y H:i') }}</span>
                      @else
                        {{ $category->id }} - {{ $category->name }}
                      @endif
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <form role="form" method="POST" action="{{ route('updateCategory', ['id' => $category->id]) }}">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input class="form-control" type="text" name="name" value="{{ $category->name }}" id="name">
                                        <p class="help-block">Name of category</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea class="form-control" cols="1" rows="10" name="description" value="{{ $category->description }}" id="description">{{ $category->description }}</textarea>
                                        <p class="help-block">Description of the category</p>
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button style="float: left;margin-right: 11px;" type="submit" class="btn btn-default" onclick="return confirm('Ready to confirm changes?')">Update Category</button>
                                </form>
                                {!!Form::open(['action' => ['AdminController@destroyCategory', 'id' => $category->id], 'method' => 'GET'])!!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    {{Form::submit('Delete Category', ['class' => 'btn btn-outline btn-danger', 'onclick' => 'return confirm("Are you sure?")'])}}
                                {!! Form::close() !!}
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->
@endsection