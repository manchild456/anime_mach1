@extends('layouts.admin')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
          <div class="page-header">
            <h3>Create A New Category</h3>
            <a href="{{ url('/admin/anime/categories') }}" class="btn btn-default btn-sm">List</a>
            <a href="{{ url('/admin/anime/category/create') }}" class="btn btn-default btn-sm btn-active">Create</a>
          </div>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-md-4">
          @include('inc.messages')
        </div>
    </div>

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                      Create Category
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <form role="form" method="POST" action="{{ route('storeCategory')}}">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input class="form-control" type="text" name="name" value="{{ old('name') }}" id="name">
                                        <p class="help-block">Name of category</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea class="form-control" cols="1" rows="10" name="description" value="{{ old('description') }}" id="description"></textarea>
                                        <p class="help-block">Description of the category</p>
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-default" onclick="return confirm('Ready to add new category?')">Add</button>
                                  </div>
                              </form>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->
@endsection