@extends('layouts.admin')

@section('content')

<div id="page-wrapper">
  <div class="row">
      <div class="col-lg-12">
        <div class="page-header">
          <h3>Episodes (List)</h3>
          <a href="{{ url('/admin/anime') }}" class="btn btn-default btn-sm btn-active">List</a>
          <a href="{{ url('/admin/anime/episodes/create') }}" class="btn btn-default btn-sm">Add</a>
        </div>
      </div>
      <!-- /.col-lg-12 -->
  </div>
      <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    All Episodes
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Sort #</th>
                                <th>Anime</th>
                                <th>Title</th>
                                <th>Sub</th>
                                <th>Dub</th>
                                <th>Status</th>
                                <th>Date Added</th>
                                <th>Date Updated</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($episodes as $episode )
                            <tr class="odd gradeX">
                            <td>{{ $episode->sort }}</td>
                            <td>{{ $episode->anm_id }}</td>
                            <td><a href="">{{ $episode->title }}</a></td>
                            <td>{{ $episode->subbed }}</td>
                            <td>{{ $episode->dubbed }}</td>
                            <td>{{ $episode->status }}</td>
                            <td>{{ $episode->created_at->format('l - F j, Y H:i') }}</td>
                            <td>{{ $episode->updated_at->format('l - F j, Y H:i') }}</td>
                            {{-- <td class="center">
                                {!!Form::open(['action' => ['AdminController@destroyAnime', 'id' => $anime->id], 'method' => 'GET'])!!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    {{Form::submit('Remove', ['class' => 'btn btn-outline btn-danger btn-sm', 'onclick' => 'return confirm("Are you sure?")'])}}
                                {!! Form::close() !!}
                            </td> --}}
                            {{-- <td class="center">
                                <a href="/anime/det/{{ $anime->slug }}/{{ $anime->id }}" class="btn btn-outline btn-primary btn-sm">Live</a>
                                <a href="/admin/anime/edit/{{ $anime->id }}" class="btn btn-outline btn-primary btn-sm">Edit</a>
                            </td> --}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@endsection