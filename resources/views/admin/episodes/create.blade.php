@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
            <div class="row">
               <div class="col-lg-12">
                <div class="page-header">
                  <h3>Episodes of </h3>
                    <a href="{{ url('/admin/anime') }}" class="btn btn-default btn-sm">List</a>
                    <a href="{{ url('/admin/anime/create') }}" class="btn btn-default btn-sm btn-active">Add</a>
                </div>
              </div>
        <!-- /.col-lg-12 -->
        <div class="col-md-4">
          @include('inc.messages')
        </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create New Anime
                        </div>
                        <div class="panel-body create-new-anime">
                            <div class="row">
                                <div class="col-lg-4">
                                    <form role="form" enctype="multipart/form-data" method="POST" action="">
                                        <div class="form-group">
                                            <label>Title<span style="color: #ff0000;" title="Required">*</span><span title="Name of anime" style="">[?]</span></label>
                                            <input class="form-control" type="text" name="title" value="{{ old('title') }}" id="title">
                                        </div>
                                        <div class="form-group">
                                            <label for="slug">Slug
                                              <span style="color: #ff0000;" title="Required">*</span>
                                              <span title="URL of anime detail page" style="">[?]</span>
                                            </label>
                                            <input class="form-control" name="slug" value="{{ old('slug') }}" id="slug" placeholder="Enter Slug">
                                            <p class="help-block">https://trueanimeclub.com/</p>
                                        </div>
                                        <div class="form-group">
                                            <label for="title_english">Title (english)<span title="english name of anime" style="">[?]</span></label>
                                            <input class="form-control" type="text" name="title_english" value="{{ old('title_english') }}" id="title_english">
                                        </div>
                                            <div class="form-group">
                                            <label for="title_japanese">Title (japanese)<span title="japanese name of anime" style="">[?]</span></label>
                                            <input class="form-control" type="text" name="title_japanese" value="{{ old('title_japanese') }}" id="title_japanese">
                                        </div>
                                            <div class="form-group">
                                            <label for="title_synonyms">Title (synonyms)<span title="synonym names of anime" style="">[?]</span></label>
                                            <input class="form-control" type="text" name="title_synonyms" value="{{ old('title_synonyms') }}" id="title_synonyms">
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Description<span title="Brief story of this anime" style="">[?]</span></label>
                                            <textarea class="form-control" rows="3" name="description" value="{{ old('description') }}" id="description"></textarea>
                                        </div>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-default">Add Anime</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </div>
                                    <div class="col-lg-4 col-md-offset-1">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td style="padding-bottom: 7px;">Status</td>
                                                    <td style="padding-bottom: 7px;">
                                                    <select name="status" class="form-control" style="width:200px;">
                                                        <option value="Ongoing">Ongoing</option>
                                                        <option value="Completed">Completed</option>
                                                        <option value="Halt">Halt</option>
                                                    </select>
                                                    </td>
                                                    <td></td>
                                                    <td style="padding-bottom: 7px;padding-left: 38px;">Aired</td>
                                                    <td><input class="form-control" name="aired" type="text" value="{{ old('aired') }}" maxlength="250" style="width:200px;margin-left: 22px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-bottom: 7px;">Rating</td>
                                                    <td style="padding-bottom: 7px;">
                                                    <select name="rating" class="form-control" style="width:200px;">
                                                        <option value="PG-13 - Teens 13 or older">PG-13 - Teens 13 or older</option>
                                                        <option value="PG-13 - Teens 13 or older">R+ - Mild Nudity</option>
                                                        <option value="PG-13 - Teens 13 or older">R - 17+ (violence & profanity)</option>
                                                        <option value="PG-13 - Teens 13 or older">G - All Ages</option>
                                                        <option value="PG-13 - Teens 13 or older">PG - Children</option>
                                                    </select>
                                                    </td>
                                                    <td></td>
                                                    <td style="padding-bottom: 7px;padding-left: 38px;">Type</td>
                                                    <td><input class="form-control" name="type" type="text" value="{{ old('type') }}" maxlength="250" style="width:200px;margin-left: 22px;"></td>
                                                </tr>
                                                <tr>
                                                    <td>Duration</td>
                                                    <td><input class="form-control" name="duration" type="text" value="{{ old('duration') }}" maxlength="250" style="width:200px;margin-left: 22px;"></td>
                                                    <td></td>
                                                    <td style="padding-bottom: 7px;padding-left: 38px;">Hot Anime?</td>
                                                    <td>
                                                        <div class="form-group" style="width:200px;margin-left: 22px;">
                                                            <label class="checkbox-inline">
                                                                <input id="isHot" name="isHot" value="1" type="checkbox">
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#Episodes</td>
                                                    <td><input class="form-control" name="episode_count" type="text" value="{{ old('episode_count') }}" maxlength="250" style="width:200px;margin-left: 22px;"></td>
                                                    <td></td>
                                                    <td style="padding-bottom: 7px;padding-left: 38px;">Related Series</td>
                                                    <td><input class="form-control" name="related_series" type="text" value="{{ old('related_series') }}" maxlength="250" style="width:200px;margin-left: 22px;"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="trailer_url" value="{{ old('trailer_url') }}" id="trailer_url" placeholder="trailer video of anime (URL)">
                                        </div>
                                        <div class="form-group">
                                            <label>Anime Poster</label>
                                            <input type="file" name="anime_poster">
                                            <a href="" onclick="document.getElementById('anime_poster').value='';document.getElementById('img_poster_prv').src=''">
                                                <small>Remove picture</small>
                                            </a>
                                            <img src="/storage/anime_poster_images/" style="width: 100%;height: auto;border: 2px solid;margin-top: 14px;"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Anime Cover</label>
                                            <input type="file" name="anime_cover">
                                            <a href="" onclick="document.getElementById('anime_cover').value='';document.getElementById('img_cover_prv').src=''">
                                                <small>Remove picture</small>
                                            </a>
                                            <img src="/storage/anime_cover_images/" style="width: 100%;height: auto;border: 2px solid;margin-top: 14px;"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
        <!-- /#page-wrapper -->
@endsection