import Vue from 'vue'
import Vuex from 'vuex'
import globalAxios from 'axios'
import {router} from './routes'

const user = window.Laravel.user;
// const animes = window.Laravel.animeList;

Vue.use(Vuex)


export default{
  state: {
    currentUser: user,
    scheduledAnime: null,
    user: null,
    animeList: null,
    bookmarks: null,
    anime: null,
    episodes: null,
    singleAnime: null,
    error: null
  },
  getters: { // computed values
      currentUser(state){
        return state.currentUser;
      },
      user(state){
        return state.user
      },
      anime(state){
        return state.animeList
      },
      singleAnime(state){
        return state.singleAnime
      },
      bookmarks(state){
        return state.bookmarks
      },
      episodes(state){
        return state.episodes
      },
      Error(state){
        return state.error;
      },
      // animes(state){
      //   return state.animes;
      // }
    },
  mutations: { // everytime you do a commit in your vue component, you will trigger a mutation function, 
    storeUser(state, user) {
      state.user = user;
    },
    storeAllAnime(state, allAnime){ // allAnime -> 'this can be called anything, the data is passed to this name'
      state.animeList = allAnime
    },
    storeSingleAnime(state, anime){
      state.singleAnime = anime
    },
    storeAllBookmarks(state, allBookmarks){
      state.bookmarks = allBookmarks
    },
    storeAllEpisodes(state, allEpisodes){
      state.episodes = allEpisodes
    },
    Error(state, errorData){
        state.error = errorData.error
    },
  },
  actions: {

    // Get a single User
    getUser({commit, dispatch}, userData){
     globalAxios.get('/api/v1/profile/u/' + userData)
        .then(res => {
          const data = res.data
          const users = []
          for (let key in data) {
            const user = data[key]
            users.push(user)
          }
          console.log(users[0])
          commit('storeUser', users)
        })
        .catch(error =>{
          commit('Error', {
            error: error
        })
      })
    },

    // Get all anime
    getAllAnime({commit, dispatch}){
      globalAxios.get('/api/v1/animes')
        .then(res => {
          console.log(res.data);
          commit("storeAllAnime", {
            animeLibrary: res.data
            // userId: res.data._id
          });
        })
    },
    
    // Get a single anime
    getAnime({commit, dispatch}, animeSlug){
      globalAxios.get('/api/v1/anime/det/' + animeSlug)
        .then(res => {
          console.log(res.data);
          commit("storeSingleAnime", {
            anime: res.data
          })
        })
    },

    // Get all Bookmarks for single User
    getAllBookmarks({commit, dispatch}, userId){
      globalAxios.get('/api/v1/bookmarks/' + userId)
      // globalAxios.get('/api/v1/bookmarks/1')
        .then(res => {
          const data = res.data
          const bookmarks = []
          for (let key in data){
            const bookmark = data[key]
            bookmarks.push(bookmark)
          }
          console.log(bookmarks)
          commit('storeAllBookmarks', bookmarks)
        })
        .catch(error =>{
          console.log(error)
        })
    },
    // Get all Episodes for single Anime
    getAllEpisodes({ commit, dispatch }, anime_id) {
      globalAxios.get('/api/v1/episodes/' + anime_id)
        // globalAxios.get('/api/v1/bookmarks/1')
        .then(res => {
          const data = res.data
          const episodes = []
          for (let key in data) {
            const episode = data[key]
            episodes.push(episode)
          }
          console.log(episodes)
          commit('storeAllEpisodes', episodes)
        })
        .catch(error => {
          console.log(error)
        })
    },
  }
}
































// export default{
//   state: {
//     user: []
//   },
//   mutations: {
//     authUser(state, user){ // logs our user in
//       state.user = user
//     }
//   },
//   actions: {
//     authCheck({commit, state}){
//       globalAxios.get('/api/v1/auth')
//         .then(res => {
//           const data = res.data
//           const users = []
//           for (let key in data){
//             const user = data[key]
//             user.id = key
//             users.push(user)
//           }
//           console.log(users)
//           commit('authUser', users[0])
//         })
//         .catch(e =>{
//           console.log(e)
//       })
//     }
//   },
//   getters: {
//     // user(state){
//     //   return state.user
//     // },
//     // isAuthenticated(state){

//     // }
//   }
// }