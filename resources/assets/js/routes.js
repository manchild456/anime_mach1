import Vue from 'vue'
import VueRouter from 'vue-router'

// Auth Components
// import Login from './components/auth/Login.vue';

// Home Components
// import HomeComponent from './components/Home/Index.vue';

// Profile Components
import BookmarkComponent from './components/User/bookmarks.vue';
// import ProfileComponent from './components/User/Profile.vue';

// Anime Components
import AnimeComponent from './components/Anime/Detail.vue';

// Inc Components
// import NavComponent from './components/Inc/NavComponent.vue';
// import FooterComponent from './components/Inc/FooterComponent.vue';




Vue.use(VueRouter)

const routes = [{
    name: "home",
    path: "/",
  },
  {
    name: "profile",
    path: "/profile/u/:display_name",
    children: [
      // BookmarkComponent will be rendered inside Profile's <router-view></router-view>
      // when /profile/u/:name is matched
      { path: 'bookmarks', component: BookmarkComponent }
    ]
  },
  {
    path: '/anime/det/:slug',
    component: AnimeComponent
  }
];


export default new VueRouter({
  mode: 'history',
  routes
})