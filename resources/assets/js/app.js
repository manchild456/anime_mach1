require('./bootstrap');

import Vue from 'vue';
import VueRouter from "vue-router";
import Vuex from 'vuex';
import axios from "axios";
import App from "./App.vue";
import router from './routes'
import StoreData from './store'
import VTooltip from 'v-tooltip';

// Auth Components
Vue.component('logout-component', require('./components/Auth/Logout.vue'));

// Inc Components
Vue.component('nav-component', require('./components/Inc/NavComponent.vue'));
Vue.component('footer-component', require('./components/Inc/FooterComponent.vue'));

// Profile Components
Vue.component('profile-component', require('./components/User/Profile.vue'));

// Anime Components
Vue.component('anime-component', require('./components/Anime/Detail.vue'));

// Home Components
Vue.component('home-component', require('./components/Home/Index.vue'));


Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(require('vue-moment'));
Vue.use(VTooltip);

const store = new Vuex.Store(StoreData);

// router.beforeEach((to, from, next) =>{
//   const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
//   const currentUser = store.state.currentUser;

//   if(requiresAuth && !currentUser){
//     next('/login');
//   }else if(to.path == '/login' && currentUser){
//     next('/');
//   }else{
//     next();
//   }
// })

// new Vue(Vue.util.extend({ router, store }, App)).$mount('#app');
const app = new Vue({
  el: '#app',
  router,
  store
});
