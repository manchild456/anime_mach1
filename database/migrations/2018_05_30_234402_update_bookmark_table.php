<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBookmarkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookmarks', function (Blueprint $table){
            $table->integer('favorite')->default(0);
            $table->float('score')->nullable();
            $table->integer('last_watched_episode')->nullable();
            $table->integer('bookmark_status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookmarks', function (Blueprint $table) {
            $table->dropColumn('favorite');
            $table->dropColumn('score');
            $table->dropColumn('last_watched_episode');
            $table->dropColumn('bookmark_status');
        });
    }
}
