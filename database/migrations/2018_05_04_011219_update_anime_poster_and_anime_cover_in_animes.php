<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAnimePosterAndAnimeCoverInAnimes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table){
            $table->string('anime_poster')->default('anime_poster_images/default_anime_poster.png');
            $table->string('anime_cover')->default('anime_cover_images/default_anime_cover.png');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('animes', function (Blueprint $table) {
            $table->dropColumn('anime_poster');
            $table->dropColumn('anime_cover');
        });
    }
}
