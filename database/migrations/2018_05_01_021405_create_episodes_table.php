<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episodes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('anime_id');
            $table->string('slug');
            $table->string('title');
            $table->integer('sort');  // sort number for this episode
            $table->string('cover')->default('episode_default_cover.png');
            $table->text('description')->nullable();
            $table->string('status')->default('Completed');
            $table->json('emb_code')->nullable();
            $table->boolean('subbed')->nullable();
            $table->boolean('dubbed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('episodes');
    }
}
