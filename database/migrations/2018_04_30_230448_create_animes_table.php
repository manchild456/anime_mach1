<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('title');
            $table->string('title_english')->nullable();
            $table->string('title_japanese')->nullable();
            $table->string('title_synonyms')->nullable();
            $table->string('anime_poster')->default('noPoster.png');
            $table->string('anime_cover')->default('noCover.png');
            $table->text('description')->nullable();
            $table->string('type')->nullable();
            $table->string('status')->default('Ongoing');
            $table->string('trailer_url')->nullable();
            $table->integer('episode_count')->nullable();
            $table->string('aired')->nullable();
            $table->string('duration')->nullable();
            $table->string('rating')->nullable();
            $table->json('categories')->nullable();      // an Array for Anime's categories
            $table->json('related_series')->nullable();  // an Array for franchise series
            $table->boolean('isHot')->default(false);    // is the Anime checked off as Hot or Not?
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animes');
    }
}
