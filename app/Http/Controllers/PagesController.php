<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Anime;

class PagesController extends Controller
{
    //



    // Return Anime Detail Page
    public function animeDetail($slug){  // make sure $slug variable comes first before $id here
        $anime = Anime::where('slug', $slug)->firstOrFail();
        return view('front.anime.animeDetail', compact('anime'));
    }
}
