<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Http\Requests;
use App\Bookmark;
use App\Anime;
use Illuminate\Http\Request;
use App\Http\Resources\Bookmark as BookmarkResource;  // since our model is called Bookmark, we'll do *as BookmarkResource

class BookmarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get All Bookmarks (Not being used * test purposes)
        $bookmarks = Bookmark::orderBy('created_at', 'desc')->paginate(5);
        return BookmarkResource::collection($bookmarks);
    }

    // Get all bookmarks by User Id
    public function getBookmarks($id){
        // Get a single user by id (get individual resource)
        // $user = Auth::user();
        // $user = User::findOrFail($id);
        // $user = User::find($id);
        $user = User::find($id);
        $bookmarks = Bookmark::where('user_id', $user->id)->orderBy('created_at', 'desc')->paginate(10);

            foreach($bookmarks as $bookmark){
                $anime = Bookmark::find($bookmark->id)->anime;
                $bookmark['anime'] = $anime;
                // $bookmarks->push($anime);
                // $bookmarks->put('anime',$anime);
                // dd($new_merge); // Log info
            }
        // $animes = Anime::all(); 
        return BookmarkResource::collection($bookmarks);
    }

    // Bookmark An Anime
    public function bookmarkAnime(Request $request){
        $user = Auth::user();

        // Create a New Anime Bookmark
        $bookmark = new Bookmark;
            // Check if bookmark record exists already
            if (Bookmark::where('user_id', '=', $user->id)->where('anime_id', '=', $request->get('anime_id'))->exists()){
                // bookmark found
                return response()->json('You already Bookmarked this anime!');
            }else{
                $bookmark->user_id = $user->id;
                $bookmark->anime_id = $request->get('anime_id');
        
                $bookmark->save();
                return response()->json(['success' => 'You have successfully Bookmarked this anime!'], 200);
            }
    }

    // Favorite A Bookmark
    public function favoriteBookmark($id){
        // Find bookmark by id
        $bookmark = Bookmark::findOrFail($id);

        if($bookmark->favorite == 1){ // if bookmark has a 1 (it's already favorited, remove it and set to 0)
            $bookmark->favorite = 0;
            $bookmark->save();
            return response()->json('Favorite Bookmark Removed');
        }else{
            $bookmark->favorite = 1;
            $bookmark->save();
            return response()->json(['success' => 'Anime Bookmark was favorited.'], 200);
        }
    }


    // Check Bookmark Status
    public function checkBookmarkStatus($slug){
        $user = Auth::user();
        $anime = Anime::where('slug', $slug)->firstOrFail();
        if(Bookmark::where('user_id', '=', $user->id)->where('anime_id', '=', $anime->id)->exists()){
            // if bookmark exist in database,
            return response()->json(['success' => 'This anime was already bookmarked!'], 200);
        }else{
            return response()->json('not bookmarked yet.');
        }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function show(Bookmark $bookmark)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function edit(Bookmark $bookmark)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bookmark $bookmark)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $anime = Anime::findOrFail($id);
        //
        $bookmark = Bookmark::where('user_id', '=', $user->id)->where('anime_id', '=', $anime->id)->firstOrFail();

        // if bookmark exist in database, delete it.
        $bookmark->delete();
        return response()->json(['success' => 'Anime Bookmark has been removed.'], 200);
    }
}
