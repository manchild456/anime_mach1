<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Auth;
use App\User;
use App\Http\Requests;
use Image;
use App\Http\Resources\User as UserResource;  // since our model is called User, we'll do *as UserResource


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get All Users
        $users = User::orderBy('created_at', 'desc')->paginate(5);
        return UserResource::collection($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get a single user (get individual resource)
        $user = User::findOrFail($id);

        // Returns a single user as a resource
        return new UserResource($user);
    }

    public function getAuthUser(){
        $authUser = Auth::user();
        return new UserResource($authUser);
    }


    public function getUserByName($display_name){
        // Get a single user by name (get individual resource)
        $user = User::where('display_name', $display_name)->firstOrFail();
        return new UserResource($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $this->validate($request, [
            'name' => 'max:100',
            'bio' => 'max:300',
            'anime_poster' => 'image|nullable|mimes:jpeg,jpg,png|max:1999',
            'anime_cover' => 'image|nullable|mimes:jpeg,jpg,png,gif|max:1999',  
        ]);

        $user = Auth::user();
        
        // Handle file upload for avatar image
        if($request->get('image')){
          $image = $request->get('image');
          $avatarFilename = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->resize(175, 175)->save(public_path('storage/user_avatar_images/').$avatarFilename);

        //    $user->avatar = $avatarFilename;
        }

        // Handle file upload for cover image
        if($request->get('cover')){
          $image = $request->get('cover');
          $coverFilename = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('cover'))->save(public_path('storage/user_cover_images/').$coverFilename);

        //   $user->cover = $coverFilename;
        }

        // Check if image, and delete old
        if($request->get('image')){
            if($user->avatar != 'default_user.png'){
                Storage::delete('public/user_avatar_images/'. $user->avatar); // delete the old image if the avatar was updated.
            }
            $user->avatar = $avatarFilename;
        }

        // Check if cover, and delete old
        if($request->get('cover')){
            if($user->avatar != 'default_user.png'){
                Storage::delete('public/user_cover_images/'. $user->cover); // delete the old image if the cover was updated.
            }
            $user->cover = $coverFilename;
        }
        
        $user->name = $request->get('name');
        $user->bio = $request->get('bio');
        $user->save();
        return response()->json(['success' => 'You have successfully uploaded an image'], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
