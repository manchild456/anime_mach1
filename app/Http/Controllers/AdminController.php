<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Hash;
use App\User;
use App\Anime;
use App\Episode;
use App\Category;

class AdminController extends Controller
{
    
    public function index(){ // ----------------------------------- main admin dashboard page ----------------------------------
        $user = Auth::user();
        $users = User::all();
        $animes = Anime::all();
        $results = User::latest('created_at')->first();
        // redirect each user type to their respective page

        if($user->role == 1){
            return view('admin.index', compact('users', 'results', 'animes'));
        }else{
            return view('welcome');
        }
    }






    public function users(){ // ----------------------------------- admin user page and crud ----------------------------------
        $user = Auth::user();
        $admincount = User::where('role','=','1')->count();
        $usercount = User::where('role','=','2')->count();
        $users = User::orderBy('display_name','asc')->paginate(30);

        if($user->role == 1){
            // return view('admin.users')->with('users' = $users, 'admins' = $admincount);
            return view('admin.users.users', compact('users', 'admincount', 'usercount'));
        }else{
            return view('welcome');
        }

    }


    // Return User Edit Page
    public function showUser($id){
        $authUser = Auth::user();
        $user = User::findOrFail($id);

        if($authUser->role == 1){
            return view('admin.users.edit', compact('user'));
        }else{
            return view('welcome');
        }
    }


    // Return User Create Page
    public function createUser(){
        return view('admin.users.create');
    }


    // Create A New User
    public function storeUser(Request $request){
        $this->validate($request, [
            'display_name' => 'required',
            'email' => 'required'
        ]);

        // Create a New User
        $user = new User;
        $user->display_name = $request->input('display_name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));

        $user->save();
        return redirect('/admin/users/edit/'.$user->id)->with('success', 'User '.$user->display_name.' has been created'); 
    }



    // Update A User
    public function updateUser(Request $request, $id){
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required'
        ]);

        // Handle file upload for avatar image
        if($request->hasfile('avatar_img')){
            $filenameWithExt = $request->file('avatar_img')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('avatar_img')->getClientOriginalExtension();
            $filenameToStore = $filename.'_'.time().'.'.$extension;

            //upload the new image
            $path = $request->file('avatar_img')->storeAs('public/user_avatar_images', $filenameToStore);
        }

        // Handle file upload for cover image
        if($request->hasfile('cover_img')){
            $filenameWithExt = $request->file('cover_img')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('cover_img')->getClientOriginalExtension();
            $filenameToStore = $filename.'_'.time().'.'.$extension;

            //upload the new image
            $path = $request->file('cover_img')->storeAs('public/user_cover_images', $filenameToStore);
        }

        // find the user
        $user = User::find($id);

        // Update the user
        $user->display_name = $request->input('display_name');
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->bio = $request->input('bio');
        if($request->hasFile('avatar_img')){
            if($user->avatar != 'default_user.png'){
                Storage::delete('public/user_avatar_images/'. $user->avatar); // delete the old image if the avatar was updated.
            }
            $user->avatar = $filenameToStore;
        }
        if($request->hasFile('cover_img')){
            if($user->cover != 'cover_default.jpeg'){
                Storage::delete('public/user_cover_images/'. $user->cover); // delete the old image if the avatar was updated.
            }
            $user->cover = $filenameToStore;
        }
        $user->save();
        return redirect()->back()->with('success', 'User '.$user->display_name.' has been updated');
    }

    // Delete A User
    public function destroyUser($id){
        // find user by id
        $user = User::findOrFail($id);

        // Delete User
        $user->delete();
        return redirect('/admin/users')->with('success', 'User '.$user->display_name.' has been removed');
    }





    public function anime(){ // ----------------------------------- admin anime page and crud ----------------------------------
        $user = Auth::user();
        $animes = Anime::orderBy('title','asc')->paginate(30);
        $category_list = Anime::all();
        // $categories_Json = Anime::all('categories');
        // $categoriesblah = json_decode($categories_Json, true); 
        

        if($user->role == 1){
            return view('admin.animes.anime', compact('animes', 'category_list'));
            // return view('admin.animes.anime')->with(['animes' => $animes, 'categoriesblah' => json_decode($categories_Json, true)]);
        }else{
            return view('welcome');
        }
    }


    // Return Anime Create Page
    public function createAnime(){
        $anime_list = Anime::all();
        return view('admin.animes.create', compact('anime_list'));
    }



     // Create A New Anime
    public function storeAnime(Request $request){
        $this->validate($request, [
            'slug' => 'required',
            'title' => 'required',
            'anime_poster' => 'image|nullable|mimes:jpeg,jpg,png|max:1999',
            'anime_cover' => 'image|nullable|mimes:jpeg,jpg,png|max:1999',  
        ]);

        
        // Handle file upload for Anime Poster
        if($request->hasfile('anime_poster')){
            $filenameWithExt = $request->file('anime_poster')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('anime_poster')->getClientOriginalExtension();
            $filenamePosterToStore = $filename.'_'.time().'.'.$extension;

            // Upload the image
            $path = $request->file('anime_poster')->storeAs('public/anime_poster_images', $filenamePosterToStore);
        }else{
            $filenamePosterToStore = 'default_anime_poster.png'; // if no logo was added, apply the default image. 
        }

        // Handle file upload for Anime Cover
        if($request->hasfile('anime_cover')){
            $filenameWithExt = $request->file('anime_cover')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('anime_cover')->getClientOriginalExtension();
            $filenameToStore = $filename.'_'.time().'.'.$extension;

            // Upload the image
            $path = $request->file('anime_cover')->storeAs('public/anime_cover_images', $filenameToStore);
        }else{
            $filenameToStore = 'default_anime_cover.png'; // if no logo was added, apply the default image. 
        }

        // Create a New Anime
        $anime = new Anime;
        $anime->title = $request->input('title');
        $anime->slug = $request->input('slug');
        $anime->title_english = $request->input('title_english');
        $anime->title_japanese = $request->input('title_japanese');
        $anime->title_synonyms = $request->input('title_synonyms');
        $anime->description = $request->input('description');
        $anime->type = $request->input('type');
        $anime->status = $request->input('status');
        $anime->trailer_url = $request->input('trailer_url');
        $anime->episode_count = $request->input('episode_count');
        $anime->aired = $request->input('aired');
        $anime->duration = $request->input('duration');
        $anime->rating = $request->input('rating');
        $anime->categories = $request->input('categories');
        $anime->related_series = $request->input('related_series');
        $anime->isHot = $request->input('isHot');

        $anime->anime_poster = $filenamePosterToStore;
        $anime->anime_cover = $filenameToStore;

        $anime->save();
        return redirect('/admin/anime/edit/'.$anime->id)->with('success', 'Anime '.$anime->title.' has been created'); 
    }


    // Return Anime Edit Page
    public function showAnime($id){
        $authUser = Auth::user();
        $anime_list = Anime::all();
        $anime = Anime::findOrFail($id);
        $category_list = Category::all();

        if($authUser->role == 1){
            return view('admin.animes.edit', compact('anime', 'anime_list', 'category_list'));
        }else{
            return view('welcome');
        }
    }

    // Update Anime
    public function UpdateAnime(Request $request, $id){
        $this->validate($request,[
            'title' => 'required',
            'slug' => 'required',
            'anime_poster' => 'image|nullable|mimes:jpeg,jpg,png|max:1999',
            'anime_cover' => 'image|nullable|mimes:jpeg,jpg,png|max:1999',  
        ]);

        // Handle file upload for anime poster image
        if($request->hasfile('anime_poster')){
            $filenameWithExt = $request->file('anime_poster')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('anime_poster')->getClientOriginalExtension();
            $filenameToStore = $filename.'_'.time().'.'.$extension;

            //upload the new image
            $path = $request->file('anime_poster')->storeAs('public/anime_poster_images', $filenameToStore);
        }

        // Handle file upload for anime cover image
        if($request->hasfile('anime_cover')){
            $filenameWithExt = $request->file('anime_cover')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('anime_cover')->getClientOriginalExtension();
            $filenameToStore = $filename.'_'.time().'.'.$extension;

            //upload the new image
            $path = $request->file('anime_cover')->storeAs('public/anime_cover_images', $filenameToStore);
        }

        // find the anime
        $anime = Anime::find($id);

        // Update the Anime
        $anime->title = $request->input('title');
        $anime->slug = $request->input('slug');
        $anime->title_english = $request->input('title_english');
        $anime->title_japanese = $request->input('title_japanese');
        $anime->title_synonyms = $request->input('title_synonyms');
        $anime->description = $request->input('description');
        $anime->type = $request->input('type');
        $anime->status = $request->input('status');
        $anime->trailer_url = $request->input('trailer_url');
        $anime->episode_count = $request->input('episode_count');
        $anime->aired = $request->input('aired');
        $anime->duration = $request->input('duration');
        $anime->rating = $request->input('rating');
        $anime->categories = $request->input('categories');
        $anime->related_series = $request->input('related_series');
        $anime->isHot = $request->input('isHot');

        if($request->hasFile('anime_poster')){
            if($anime->anime_poster != 'default_anime_poster.png'){
                Storage::delete('public/anime_poster_images/'. $anime->anime_poster); // delete the old image if the poster was updated.
                $anime->anime_poster = $filenameToStore;
            }
        }else if($request->hasFile('anime_cover')){
           if($anime->anime_cover != 'default_anime_cover.png'){
                Storage::delete('public/anime_cover_images/'. $anime->anime_cover); // delete the old image if the cover was updated.
                $anime->anime_cover = $filenameToStore;
            }
        }
        $anime->save();
        return redirect()->back()->with('success', $anime->title.' has been updated');
    }


    public function episode(){ // ----------------------------------- admin episode page and crud ----------------------------------
        $user = Auth::user();
        $episodes = Episode::all();

        if($user->role == 1){
            return view('admin.episodes.episodes', compact('episodes', 'animes'));
        }else{
            return view('welcome');
        }
    }


    // Return Episode Create Page
    public function createEpisode(){
        return view('admin.episodes.create');
    }


    // Return Anime Edit Page
    public function showEpisode($id){
        $authUser = Auth::user();
        $episode = Episode::findOrFail($id);
        $anime = Anime::all();

        if($authUser->role == 1){
            return view('admin.episodes.edit', compact('anime', 'episode'));
        }else{
            return view('welcome');
        }
    }


     // Create A New Episode
    public function storeEpisode(Request $request){
        $this->validate($request, [
            'slug' => 'required',
            'title' => 'required',
            'anime_poster' => 'image|nullable|mimes:jpeg,jpg,png|max:1999',
            'anime_cover' => 'image|nullable|mimes:jpeg,jpg,png|max:1999',  
        ]);

        
        // Handle file upload for Anime Poster
        if($request->hasfile('anime_poster')){
            $filenameWithExt = $request->file('anime_poster')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('anime_poster')->getClientOriginalExtension();
            $filenameToStore = $filename.'_'.time().'.'.$extension;

            // Upload the image
            $path = $request->file('anime_poster')->storeAs('public/anime_poster_images', $filenameToStore);
        }else{
            $filenameToStore = 'default_anime_poster.png'; // if no logo was added, apply the default image. 
        }

        // Handle file upload for Anime Cover
        if($request->hasfile('anime_cover')){
            $filenameWithExt = $request->file('anime_cover')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('anime_cover')->getClientOriginalExtension();
            $filenameToStore = $filename.'_'.time().'.'.$extension;

            // Upload the image
            $path = $request->file('anime_cover')->storeAs('public/anime_cover_images', $filenameToStore);
        }else{
            $filenameToStore = 'default_anime_cover.png'; // if no logo was added, apply the default image. 
        }

        // Create a New Anime
        $anime = new Anime;
        $anime->title = $request->input('title');
        $anime->slug = $request->input('slug');
        $anime->title_english = $request->input('title_english');
        $anime->title_japanese = $request->input('title_japanese');
        $anime->title_synonyms = $request->input('title_synonyms');
        $anime->description = $request->input('description');
        $anime->type = $request->input('type');
        $anime->status = $request->input('status');
        $anime->trailer_url = $request->input('trailer_url');
        $anime->episode_count = $request->input('episode_count');
        $anime->aired = $request->input('aired');
        $anime->duration = $request->input('duration');
        $anime->rating = $request->input('rating');
        $anime->categories = $request->input('categories');
        $anime->related_series = $request->input('related_series');
        $anime->isHot = $request->input('isHot');

        $anime->anime_poster = $filenameToStore;
        $anime->anime_cover = $filenameToStore;

        $anime->save();
        return redirect('/admin/anime/edit/'.$anime->id)->with('success', 'Anime '.$anime->title.' has been created'); 
    }




    public function categories(){ // ----------------------------------- admin categories page and crud ----------------------------------
        $categories = Category::orderBy('name')->paginate(30);
        return view('admin.categories.index', compact('categories'));
    }


    public function createCategory(){
        return view('admin.categories.create');
    }

    public function storeCategory(Request $request){
        $this->validate($request, [
            'name' => 'required'
        ]);

        // Create Category
        $category = new Category;
        $category->name = $request->input('name');
        $category->description = $request->input('description');

        $category->save();
        return redirect('/admin/anime/category/edit/'.$category->id)->with('success', 'Category '.$category->name.' has been added');
    }


    public function showCategory($id){
        $category = Category::findOrFail($id);
        return view('admin.categories.edit', compact('category'));
    }


    public function updateCategory(Request $request, $id){
        $this->validate($request, [
            'name' => 'required'
        ]);
        
        // find Category
        $category = Category::find($id);

        // Update Category
        $category->name = $request->input('name');
        $category->description = $request->input('description');

        $category->save();
        return redirect('/admin/anime/category/edit/'.$category->id)->with('success', 'Category: '.$category->name.' has been updated');
    }

    public function destroyCategory($id){
        $category = Category::findOrFail($id);

        // Delete Category
        $category->delete();
        return redirect('/admin/anime/categories')->with('success', 'Category '.$category->name.' has been removed');
    }








}
