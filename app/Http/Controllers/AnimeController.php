<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Anime;
use App\Http\Resources\Anime as AnimeResource;  // since our model is called Anime, we'll do *as AnimeResource


class AnimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Get All Anime
        $animes = Anime::orderby('created_at', 'desc')->paginate(43);

        // Return collection of animes as a resource
        return AnimeResource::collection($animes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Anime  $anime
     * @return \Illuminate\Http\Response
     */
    public function show(Anime $slug)
    {
        //
    }


    // Get Anime by Slug Name
    public function getAnimeBySlug($slug){
        // Get a single anime by name (get individual resource)
        $anime = Anime::where('slug', $slug)->firstOrFail();
        return new AnimeResource($anime);
    }


    // public function getAnimeBySlug($slug, $id){
    //     // Get a single anime by name (get individual resource)
    //     $anime = Anime::where('slug', $slug)->where('id', $id)->firstOrFail();
    //     return new AnimeResource($anime);
    // }

    
    // Get Episodes by Anime id
    public function getEpisodes($id){
        
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Anime  $anime
     * @return \Illuminate\Http\Response
     */
    public function edit(Anime $anime)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Anime  $anime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Anime $anime)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Anime  $anime
     * @return \Illuminate\Http\Response
     */
    public function destroy(Anime $anime)
    {
        //
    }
}
