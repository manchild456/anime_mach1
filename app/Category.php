<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'description'];

    // Primary Key
    public $primaryKey = 'id';

    // A Category belongs to many anime
    public function anime(){
        return $this->hasMany('App\Anime');
    }
}
