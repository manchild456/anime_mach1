<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anime extends Model
{
    protected $fillable = [ //← only the field names inside the array can be mass-assign
        'slug', 'title', 'title_english', 'title_japanese', '', 'title_synonyms', 'anime_poster', 'anime_cover', 
        'description', 'type', 'aired', 'status', 'duration', 'rating', 'episode_count', 
        'trailer_url', 'isHot'
    ];

    protected $casts = [
        'categories' => 'array',
        'related_series' => 'array'
    ];

    // Table Name
    protected $table = 'animes';

    // Primary Key
    public $primaryKey = 'id';

    // Timestamps
    public $timestamps = true;


    // An Anime has many episodes
    public function episodes(){  // creating a relationship between Anime and Episodes
        return $this->hasMany('App\Episode');
    }

    // An Anime has many categories
    public function categories(){
        return $this->belongsToMany('App\Category');
    }
}
