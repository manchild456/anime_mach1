<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    // An Anime belongs to one Bookmark
    public function anime(){
        return $this->belongsTo('App\Anime');
    }

    // An User belongs to one Bookmark
    public function user(){
        return $this->belongsTo('App\User');
    }
}