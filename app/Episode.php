<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
    // An Episode belongs to one Anime
    public function anime(){ // creating a relationship between Anime and Episode
        return $this->belongsTo('App\Anime');
    }
}
